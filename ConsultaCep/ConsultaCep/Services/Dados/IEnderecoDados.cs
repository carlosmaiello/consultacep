﻿using System;
using System.Collections.Generic;
using System.Text;
using ConsultaCep.Models;

namespace ConsultaCep.Services.Dados
{
    public interface IEnderecoDados
    {
        Endereco Consultar(string cep);
    }
}
