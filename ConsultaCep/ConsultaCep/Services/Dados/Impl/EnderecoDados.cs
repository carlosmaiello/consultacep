﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Text;
using ConsultaCep.Models;
using Newtonsoft.Json;

namespace ConsultaCep.Services.Dados.Impl
{
    public class EnderecoDados : IEnderecoDados
    {
        public Endereco Consultar(string cep)
        {
            string url = "https://viacep.com.br/ws/"+ cep +"/json/";
            WebClient client = new WebClient();
            string retorno = client.DownloadString(url);

            return JsonConvert.DeserializeObject<Endereco>(retorno);
        }
    }
}
