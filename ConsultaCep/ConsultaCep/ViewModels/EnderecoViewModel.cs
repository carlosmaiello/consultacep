﻿using ConsultaCep.Services.Dados.Impl;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Text;
using System.Windows.Input;
using Xamarin.Forms;

namespace ConsultaCep.ViewModels
{
    public class EnderecoViewModel : INotifyPropertyChanged
    {
        private string _cep;
        private string _logradouro;
        private ICommand _consultarCommand;
        private string _complemento;
        private string _bairro;
        private string _localidade;
        private string _uf;
        private string _unidade;
        private string _ibge;
        private string _gia;

        public string Cep
        {
            get => _cep;
            set { _cep = value; OnPropertyChanged("Cep"); }
        }
        public string Logradouro
        {
            get => _logradouro;
            set { _logradouro = value; OnPropertyChanged("Logradouro"); }
        }
        public string Complemento { get => _complemento; set { _complemento = value; OnPropertyChanged("Complemento"); } }
        public string Bairro { get => _bairro; set { _bairro = value; OnPropertyChanged("Bairro"); } }
        public string Localidade { get => _localidade; set { _localidade = value; OnPropertyChanged("Localidade"); } }
        public string Uf { get => _uf; set { _uf = value; OnPropertyChanged("Uf"); } }
        public string Unidade { get => _unidade; set { _unidade = value; OnPropertyChanged("Unidade"); } }
        public string Ibge { get => _ibge; set { _ibge = value; OnPropertyChanged("Ibge"); } }
        public string Gia { get => _gia; set { _gia = value; OnPropertyChanged("Gia"); } }

        public ICommand ConsultarCommand { get => _consultarCommand; set => _consultarCommand = value; }

        public event PropertyChangedEventHandler PropertyChanged;

        protected virtual void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this,
                new PropertyChangedEventArgs(propertyName));
        }

        public EnderecoViewModel()
        {
            ConsultarCommand = new Command(() =>
            {
                var dados = new EnderecoDados();
                var endereco = dados.Consultar(this.Cep);

                this.Logradouro = endereco.Logradouro;
                this.Bairro = endereco.Bairro;
                this.Complemento = endereco.Complemento;
                this.Gia = endereco.Gia;
                this.Ibge = endereco.Ibge;
                this.Localidade = endereco.Localidade;
                this.Unidade = endereco.Unidade;
            });
        }
    }
}
