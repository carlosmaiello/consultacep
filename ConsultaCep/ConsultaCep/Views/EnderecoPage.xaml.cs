﻿using ConsultaCep.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace ConsultaCep.Views
{
	[XamlCompilation(XamlCompilationOptions.Compile)]
	public partial class EnderecoPage : ContentPage
	{
		public EnderecoPage ()
		{
			InitializeComponent ();
            BindingContext = new EnderecoViewModel();
		}
	}
}